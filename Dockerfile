FROM php:7.0-apache

RUN docker-php-ext-install mysql

ENTRYPOINT ["/usr/sbin/httpd","-D","FOREGROUND"]
